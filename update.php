<?php
//================================================================ REMOVE EXISTING FILES
$files = glob('images/*'); // get all file names
foreach($files as $file){ // iterate files
  if(is_file($file)) {
    unlink($file); // delete file
  }
}

//======================= QUERY NOTION DATABASE
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.notion.com/v1/databases/XXX/query',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_HTTPHEADER => array(
    'Notion-Version: 2021-08-16',
    'Authorization: Bearer secret_XXX'
  ),
));
$response = curl_exec($curl);
curl_close($curl);
$database = json_decode($response);

$properties = [];
foreach ($database->results as $item) {
  $word = $item->properties->mot->title[0]->plain_text;
  $link = $item->properties->lien->rich_text[0]->text->link->url;
  $image = $item->properties->image->files[0]->file->url;

  $imageName = strrchr($image, "/");
  $imageName = substr($imageName, 1, strlen($imageName));
  $imageName = strtok($imageName, "?");
  
  $content = file_get_contents($image);
  file_put_contents('images/' . $imageName, $content);

  $properties[] = [
    'word' => $word,
    'link' => $link,
    'image' => './images/' . $imageName
  ];
}

$dataFile = fopen('./data.json', 'w');
fwrite($dataFile, json_encode($properties));
fclose($dataFile);
