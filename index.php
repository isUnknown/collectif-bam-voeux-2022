<?php
  $savedData = file_get_contents('./data.json');
  $savedData = json_decode($savedData);

  $properties = $savedData;
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bonne année 2022 !</title>
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <div id="sentence">
    <h4>Le <strong>Collectif Bam</strong> vous souhaite <br />
    une <strong>année 2022 <span id="word-desktop">praticable</span><a href="https://collectifbam.fr/projets/realisations" title="praticable" id="word-mobile" target="_blank">pratic-cable</a> !</strong></h4>
  </div>
  <div id="grid">
    <?php shuffle($properties);
      for($i=0; $i < 21; $i++) :?>
      <a class="card-link" href="<?= $properties[$i]->link ?>" title="<?= $properties[$i]->word ?>" target="_blank">
        <img src="<?= $properties[$i]->image ?>" alt="">
      </a>
    <?php endfor; ?>

    </div>
  
  <script>
    // =========================================== GLOBAL VARIABLES
    let random = Math.floor(Math.random() * 15) + 1 // FOR RANDOM AUTOMATIC SWITCH ON MOBILE
    const word = window.innerWidth > 850 ? document.querySelector('#word-desktop') : document.querySelector('#word-mobile')

    // =========================================== FUNCTIONS
    const switchImageMobile = (randomCard, image) => {
      image.style.opacity = '1'
      randomCard.style.pointerEvents = 'all'
      setTimeout(() => {
        image.style.opacity = '0'
        randomCard.style.pointerEvents = ''
      }, 1990);
    }

    const switchWordMobile = (randomCard, image) => {
      const title = randomCard.getAttribute('title')
      const link = randomCard.href

      word.textContent = title
      word.title = title
      word.href = link
    }

    const getRandomCard = () => {
      const cards = document.querySelectorAll('#grid a')
      let newRandom = Math.floor(Math.random() * 15)
      if (newRandom === random) {
        random < 14 && random > 2 ? random += 2 : random -= 2
      } else {
        random = newRandom
      }

      console.log('random :', random)
      
      const card = cards[random]
      return card
    }

    const switchWordDesktop = event => {
      const target = event.target
      const title = target.getAttribute('title')

      word.textContent = title
    }

    const moveFirstCardToLast = () => {
      const firstCard = document.querySelector('#grid a')
      const grid = document.querySelector('#grid')

      grid.removeChild(firstCard)
      grid.appendChild(firstCard)
    }

    // =========================================== TRIGGERS
    if (window.innerWidth > 850) {
      const cards = document.querySelectorAll('#grid a')
      cards.forEach(card => {
        card.addEventListener('mouseenter', switchWordDesktop)
      })
    } else {
      setInterval(() => {
        // moveFirstCardToLast()
        const randomCard = getRandomCard()
        const image = randomCard.querySelector('img')
        console.log(randomCard)
        switchWordMobile(randomCard, image)
        switchImageMobile(randomCard, image)
      }, 2000)
    }
  </script>
</body>

</html>