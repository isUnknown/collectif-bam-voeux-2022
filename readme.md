# Carte de vœux numérique 2022 du Collectif Bam.

## Récupération du contenu d'une page Notion via l'API Notion.
La page hérite du contenu d'une page Notion. 
Pour connecter votre propre database Notion, créez une intégration pour votre database Notion via https://www.notion.com/my-integrations
Puis dans le fichier update.php, remplacez le placeholder (XXX) de la ligne 13 :
```
CURLOPT_URL => 'https://api.notion.com/v1/databases/XXX/query'
```
par l'identifiant de votre database, situé dans l'url du block :
```
https://www.notion.so/myworkspace/a8aec43384f447ed84390e8e42c2e089?v=...
                                  |--------- Database ID --------|
```
et le placeholder (XXX) de la ligne 23 :
```
'Authorization: Bearer secret_XXX'
```
par l'Internal Integration Token de votre intégration.

## Page semi-statique
Pour éviter d'avoir à joindre à chaque requête l'API Notion, la page stocke les données sur son serveur. Il faut déclencher la récupération manuellement à chaque fois que vous souhaitez mettre à jour la page en vous rendant sur la page [www.url/de/la/page/update.html](url). La récupération peut prendre quelques minutes, selon la quantité et le poids des ressources.

## Génération aléatoire de la grille de resources
En version desktop, la page affiche 20 ressources (5x4). En version mobile, elle en affiche 21 (3x7). Il ne faut donc pas mettre moins de 21 ressources (mot / url / image) dans la database. Il est néanmoins possible d'en mettre davantage dans la mesure où la sélection de 21 ressources (parmi toutes celles disponibles) est générée aléatoirement à chaque requête.
